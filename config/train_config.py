class TrainConfig(type):
    INPUT_SHAPE = [500, 500, 3]
    MAX_BOX_PER_IMAGE = 10

    BATCH_SIZE = 8
    TRAIN_EPOCH = 1

    SUMMARIES_PER_EPOCH = 10
    IMAGES_PER_EPOCH = 100
    IMAGE_MAX_OUTPUT = 3
    SKIP_FIRST_EPOCHS = 0

    IMAGE_PRE_NMS = False
    IMAGE_ANCHORS = False

    MAX_CHECKPOINT = 3

    LEARNING_RATE = 0.001

    LABEL_MATCH_IOU = 0.5  # Minimal IoU to match a label with an anchor
    HARD_NEGATIVE_MINING_RATIO = 2  # Ratio of negative box to use compared to positives (otherwise to many negative)
