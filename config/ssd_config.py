class SSDConfig(type):
    LAYER_FEATURES = [64, 64, 64, 64]
    LAYER_RATIOS = [[1, 3/2, 2/3]] * len(LAYER_FEATURES)

    BOX_SIZE_FACTOR = 1

    LOCATION_DIMMENSION = 4
    OUTPUT_CLASSES = 20

    CONFIDENCE_THRESHOLD = 0.5
    NMS_IOU = 0.5
