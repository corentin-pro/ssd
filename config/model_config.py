class ModelConfig(type):
    CONV1_KERNEL_SIZE = 5
    CONV1_FEATURES = 32
    CONV2_FEATURES = 64
    CONV3_FEATURES = 128
    CONV4_FEATURES = 128
    CONV5_FEATURES = 256
