import argparse
import os

from src.infer import infer


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('data_path', help='Path to the folder containing the test images')
    parser.add_argument('checkpoint_path', help='Path to checkpoint to load')
    parser.add_argument('--output-dir', nargs='?', default='test',
                        help='Data directory where output files will be saved')
    arguments = parser.parse_args()

    os.makedirs(arguments.output_dir, exist_ok=True)

    file_list = os.listdir(arguments.data_path)
    test_files = []
    for file_name in file_list:
        if file_name[9] == '.':
            test_files.append(os.path.join(arguments.data_path, file_name))
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
    infer(test_files, 1, arguments.checkpoint_path, arguments.output_dir, testing_output=True)


if __name__ == '__main__':
    main()
