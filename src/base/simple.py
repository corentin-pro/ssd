import src.layers as layers
from src.network import Network


class SimpleNetwork(Network):

    def __init__(self, input_op, is_training=None, batch_norm_training=None):
        self.input_op = input_op
        self.layers = []

        if is_training is not None:
            layers.Layer.IS_TRAINING = is_training
        if batch_norm_training is not None:
            layers.Layer.BATCH_NORM_TRAINING = batch_norm_training

        output_op = self.conv2D(input_op, 16, strides=[1, 2, 2, 1])
        output_op = self.conv2D(output_op, 32, strides=[1, 2, 2, 1])
        output_op = self.conv2D(output_op, 64, strides=[1, 2, 2, 1])
        output_op = self.conv2D(output_op, 64, strides=[1, 2, 2, 1])
        output_op = self.conv2D(output_op, 128, strides=[1, 2, 2, 1])
        output_op = self.conv2D(output_op, 128, strides=[1, 2, 2, 1])
        self.output_op = output_op
