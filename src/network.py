import tensorflow as tf

import src.layers as layers


class Network():

    def conv1D(self, input_op, output_features, **kwargs):
        convolution = layers.Conv1D(input_op, output_features, **kwargs)
        self.layers.append(convolution)
        return convolution.output_op

    def conv2D(self, input_op, output_features, **kwargs):
        convolution = layers.Conv2D(input_op, output_features, **kwargs)
        self.layers.append(convolution)
        return convolution.output_op

    def smartConv2D(self, input_op, output_features, name='SmartConv',
                    strides=[1, 1, 1, 1], kernel_size=3, **kwargs):
        with tf.variable_scope(name):
            convolution = layers.Conv2D(input_op, output_features, kernel_size=kernel_size, strides=strides,
                                        name='%dx%d' % (kernel_size, kernel_size), **kwargs)
            self.layers.append(convolution)
            convolution = layers.Conv2D(convolution.output_op, output_features, kernel_size=1, name='1x1', **kwargs)
        return convolution.output_op

    def deconv2D(self, input_op, **kwargs):
        deconvolution = layers.DeConv2D(input_op, **kwargs)
        self.layers.append(deconvolution)
        return deconvolution.output_op

    def smartDeconv2D(self, input_op, output_shape=None, name='SmartDeconv',
                      strides=[1, 1, 1, 1], kernel_size=3, **kwargs):
        with tf.variable_scope(name):
            deconvolution = layers.DeConv2D(
                input_op, output_shape=output_shape, kernel_size=kernel_size, strides=strides,
                name='%dx%d' % (kernel_size, kernel_size), **kwargs)
            self.layers.append(deconvolution)
            output_shape = deconvolution.output_op.get_shape().as_list()
            deconvolution = layers.Conv2D(deconvolution.output_op, output_shape[-1],
                                          kernel_size=1, name='1x1', **kwargs)
        return deconvolution.output_op

    def skip_connection(self, input_op, skip_op, output_features=None, name="SkipCon", **kwargs):
        if not output_features:
            output_features = input_op.get_shape().as_list()[-1]
        with tf.variable_scope(name):
            output_layer = layers.Conv2D(
                tf.concat([input_op, skip_op], axis=-1), output_features, kernel_size=1, name="1x1", **kwargs)
        return output_layer.output_op

    def fully_connected(self, input_op, output_dimmension, **kwargs):
        fc = layers.FullyConnected(input_op, output_dimmension, **kwargs)
        self.layers.append(fc)
        return fc.output_op

    def lstm(self, output_dimmension, **kwargs):
        lstm = layers.LSTM(output_dimmension, **kwargs)
        self.layers.append(lstm)
        self.lstm_layers.append(lstm)
        return lstm
