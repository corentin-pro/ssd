import tensorflow as tf

from src.layers import Layer
from src.network import Network


class SSDNetwork(Network):

    def __init__(self, input_op, output_classes, location_dimension, layer_features, layer_ratios,
                 is_training=None, batch_norm_training=None):
        self.input_op = input_op  # input_op should be the base's output
        self.output_classes = output_classes  # detection classes
        self.location_dimension = location_dimension  # detection location dimension (x, y, w, h,...)
        self.layers = []
        self.detectors = []  # list of layers outputing detections

        if is_training is not None:
            Layer.IS_TRAINING = is_training
        if batch_norm_training is not None:
            Layer.BATCH_NORM_TRAINING = batch_norm_training

        if len(layer_features) != len(layer_ratios):
            raise Exception(
                'SSDNetwork arguments error: layer_features and layer_ratios should have same length : {} != {}'.format(
                    len(layer_features), len(layer_ratios)))
        output_op = input_op
        for layer_index in range(len(layer_features)):
            output_op = self.AnchorConv(
                output_op, layer_features[layer_index], layer_ratios[layer_index], strides=[1, 2, 2, 1])

        flatten_outputs = []
        for detector in self.detectors:
            detector_shape = detector.shape.as_list()
            flatten_outputs.append(tf.reshape(
                detector,
                [-1,
                 detector_shape[1] * detector_shape[2] * len(layer_ratios[0]),
                 location_dimension + output_classes]))
        self.output_op = tf.concat(flatten_outputs, axis=1, name='output')
        if Layer.VERBOSE:
            Layer.LOGGER.info('SSD outputs : {}'.format(self.output_op))

    def AnchorConv(self, input_op, output_features, ratios, **kwargs):
        """Convolution layer with output for detections (anchor boxes)"""
        with tf.variable_scope(None, default_name='AnchorConv') as layer_scope:
            if Layer.VERBOSE:
                Layer.LOGGER.info(layer_scope.name.split('/')[-1])
            self.detectors.append(self.conv2D(
                input_op, len(ratios) * (self.output_classes + 4),
                batch_norm=False, activation=tf.nn.sigmoid, name="Detector"))
            output_op = self.conv2D(input_op, output_features, kernel_size=1)
            return self.conv2D(output_op, output_features, **kwargs)
