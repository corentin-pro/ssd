import numpy as np


def calculate_anchors(image_shape, detector_output_shape, ratio_list):
    anchors = []
    image_width = image_shape[1]
    image_height = image_shape[0]
    stride_x = image_width / detector_output_shape[0]
    stride_y = image_height / detector_output_shape[1]
    for ratio in ratio_list:
        anchor_height = int(stride_y / ratio)
        anchor_center_y = int(stride_y / 2)
        for _ in range(detector_output_shape[0]):
            anchor_width = int(stride_x * ratio)
            anchor_y = int(anchor_center_y - (anchor_height / 2))
            anchor_center_x = int(stride_x / 2)
            for _ in range(detector_output_shape[1]):
                anchor_x = int(anchor_center_x - (anchor_width / 2))
                anchors.append(np.asarray(
                    check_rectangle(
                        image_shape,
                        anchor_x, anchor_y,
                        anchor_width, anchor_height)))
                anchor_center_x += stride_x
            anchor_center_y += stride_y
    return np.asarray(anchors)


def check_rectangle(image_shape, x, y, width, height):
    x_min = x
    y_min = y
    x_max = x + width - 1
    y_max = y + height - 1
    if x_min < 0:
        x_min = 0
    if y_min < 0:
        y_min = 0
    if x_max >= image_shape[1]:
        x_max = image_shape[1] - 1
    if y_max >= image_shape[0]:
        y_max = image_shape[0] - 1
    return x_min, y_min, x_max - x_min + 1, y_max - y_min + 1


def draw_rectangle(image, x, y, width, height, color=(255, 0, 0)):
    x_min = x
    y_min = y
    x_max = x + width - 1
    y_max = y + height - 1
    draw_box(image, x_min, y_min, x_max, y_max, color=color)


def draw_box(image, x_min, y_min, x_max, y_max, color=(255, 0, 0)):
    if x_min < 0:
        x_min = 0
    if y_min < 0:
        y_min = 0
    if x_max >= image.shape[1]:
        x_max = image.shape[1] - 1
    if y_max >= image.shape[0]:
        y_max = image.shape[0] - 1
    # top line
    image[y_max, x_min:x_max, 0] = color[0]
    image[y_max, x_min:x_max, 1] = color[1]
    image[y_max, x_min:x_max, 2] = color[2]
    # right line
    image[y_min:y_max, x_max, 0] = color[0]
    image[y_min:y_max, x_max, 1] = color[1]
    image[y_min:y_max, x_max, 2] = color[2]
    # bottom line
    image[y_min, x_min:x_max, 0] = color[0]
    image[y_min, x_min:x_max, 1] = color[1]
    image[y_min, x_min:x_max, 2] = color[2]
    # left line
    image[y_min:y_max, x_min, 0] = color[0]
    image[y_min:y_max, x_min, 1] = color[1]
    image[y_min:y_max, x_min, 2] = color[2]
    return image


def render_anchors(images, image_shape, detector_output_shape, ratio_list, size_factor):
    image_width = image_shape[1]
    image_height = image_shape[0]
    stride_x = image_width / detector_output_shape[0]
    stride_y = image_height / detector_output_shape[1]
    # logger.debug('Draw anchors : {} {}, default size {}x{}, stride {} {}'.format(
    #     image_shape, detector_output_shape, int(stride_x), int(stride_y), stride_x, stride_y))
    colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255)]
    for ratio_index, ratio in enumerate(ratio_list):
        anchor_height = int(size_factor * stride_y / ratio)
        for data_index, image in enumerate(images):
            anchor_center_y = int(stride_y / 2)
            for _ in range(detector_output_shape[0]):
                anchor_width = int(size_factor * stride_x * ratio)
                anchor_y = int(anchor_center_y - (anchor_height / 2))
                anchor_center_x = int(stride_x / 2)
                for _ in range(detector_output_shape[1]):
                    anchor_x = int(anchor_center_x - (anchor_width / 2))
                    # if anchor_width > 50:
                    #     logger.debug('Drawing anchor : {}'.format(
                    #         [anchor_x, anchor_y,
                    #          anchor_width, anchor_height]))
                    draw_rectangle(
                        image,
                        anchor_x, anchor_y,
                        anchor_width, anchor_height,
                        color=colors[ratio_index])
                    anchor_center_x += stride_x
                anchor_center_y += stride_y
    return images


def render_anchors_box(images, anchors):
    colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255)]
    color_modulo = len(anchors) // len(colors)
    for image in images:
        for index, anchor in enumerate(anchors):
            draw_rectangle(image, *anchor, color=colors[index // color_modulo])
    return images


def render_bounding_box(images, labels):
    # Assuming label is [x_min, y_min, width, height, class_0, class_1...]
    for image_index, image in enumerate(images):
        for label in labels[image_index]:
            draw_rectangle(image, *label[:4])
    return images


def render_box(images, boxes):
    # Assuming box is [x_min, y_min, x_max, y_max, class_0]
    for image_index, image in enumerate(images):
        for box in boxes[image_index]:
            draw_box(image, *(box[:4].astype(int)))
    return images


def get_boxes(predictions, anchors, class_index):
    box_anchors = np.copy(anchors)
    box_anchors[:, 2] = anchors[:, 0] + anchors[:, 2]
    box_anchors[:, 3] = anchors[:, 1] + anchors[:, 3]
    return np.concatenate(
        [box_anchors + predictions[:, :4], np.expand_dims(predictions[:, class_index], 1)], -1)


def fast_nms(boxes, min_iou):
    # print(boxes[0:10])
    # if there are no boxes, return an empty list
    if len(boxes) == 0:
        return []

    # if the bounding boxes integers, convert them to floats --
    # this is important since we'll be doing a bunch of divisions
    if boxes.dtype.kind == "i":
        boxes = boxes.astype("float")

    # initialize the list of picked indexes
    pick = []

    # grab the coordinates of the bounding boxes
    x1 = boxes[:, 0]
    y1 = boxes[:, 1]
    x2 = boxes[:, 2]
    y2 = boxes[:, 3]
    scores = boxes[:, 4]

    # compute the area of the bounding boxes and sort the bounding boxes by the scores
    area = (x2 - x1 + 1) * (y2 - y1 + 1)
    idxs = np.argsort(scores)

    # keep looping while some indexes still remain in the indexes
    # list
    while len(idxs) > 0:
        # grab the last index in the indexes list and add the
        # index value to the list of picked indexes
        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)

        # find the largest (x, y) coordinates for the start of
        # the bounding box and the smallest (x, y) coordinates
        # for the end of the bounding box
        xx1 = np.maximum(x1[i], x1[idxs[:last]])
        yy1 = np.maximum(y1[i], y1[idxs[:last]])
        xx2 = np.minimum(x2[i], x2[idxs[:last]])
        yy2 = np.minimum(y2[i], y2[idxs[:last]])

        # compute the width and height of the bounding box
        w = np.maximum(0, xx2 - xx1 + 1)
        h = np.maximum(0, yy2 - yy1 + 1)

        # compute the ratio of overlap
        overlap = (w * h) / area[idxs[:last]]

        # delete all indexes from the index list that have
        idxs = np.delete(
            idxs, np.concatenate(([last], np.where(overlap > min_iou)[0])))

    # return only the bounding boxes that were picked using the
    # integer data type
    return boxes[pick]
