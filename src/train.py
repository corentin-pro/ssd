import os
import resource
import shutil
import subprocess
import time

import numpy as np

from config.ssd_config import SSDConfig
from config.train_config import TrainConfig
import src.layers as layers
from src.ssd import SSDNetwork
from src.base.simple import SimpleNetwork
from src.utils.logger import DummyLogger
from src.utils.box import calculate_anchors, render_anchors_box, render_box, render_bounding_box, get_boxes, fast_nms


def train(train_data, train_labels, args, logger=DummyLogger()):
    import tensorflow as tf
    # manage memory(True->if we need then use, False->use all.This is same as default)
    gpu_options = tf.GPUOptions(allow_growth=True)
    session_config = tf.ConfigProto(gpu_options=gpu_options)

    data_shape = train_data[0].shape
    # label_dimension = train_labels[0].shape[-1]
    with tf.Session(config=session_config).as_default() as session:
        # Setting the placeholders
        data_placeholder = tf.placeholder(
            tf.float32,
            shape=[None, *data_shape],
            name='data_placeholder')
        # label_placeholder = tf.placeholder(
        #     tf.float32,
        #     shape=[None, TrainConfig.MAX_BOX_PER_IMAGE, label_dimension],
        #     name='label_placeholder')
        batch_norm_placeholder = tf.placeholder(tf.bool, shape=[], name='batch_norm_training')

        train_summaries = []
        image_summaries = []
        anchor_summaries = []

        with tf.variable_scope('Pre_Process'):
            processed_data_op = data_placeholder / 255
            # processed_label_op = label_placeholder / data_shape[0]

        with tf.variable_scope('Network'):
            layers.Layer.VERBOSE = 1
            layers.Layer.LOGGER = logger
            layers.Layer.METRICS = True
            layers.Layer.ACTIVATION = tf.nn.leaky_relu

            with tf.variable_scope('Base') as scope:
                if layers.Layer.VERBOSE:
                    logger.info(scope.name.split('/')[-1])
                base_network = SimpleNetwork(
                    processed_data_op, is_training=True, batch_norm_training=batch_norm_placeholder)
            with tf.variable_scope('SSD') as scope:
                if layers.Layer.VERBOSE:
                    logger.info(scope.name.split('/')[-1])
                ssd_network = SSDNetwork(
                    base_network.output_op,
                    SSDConfig.OUTPUT_CLASSES, SSDConfig.LOCATION_DIMMENSION,
                    SSDConfig.LAYER_FEATURES, SSDConfig.LAYER_RATIOS,
                    is_training=True, batch_norm_training=batch_norm_placeholder)
            network_output_op = ssd_network.output_op

        with tf.variable_scope('Metrics'):
            with tf.variable_scope('_Images'):
                label_image_placeholder = tf.placeholder(
                    tf.float32,
                    shape=[TrainConfig.IMAGE_MAX_OUTPUT, *data_shape],
                    name='label_image_placeholder')
                image_summaries.append(tf.summary.image(
                    'label_image', label_image_placeholder, max_outputs=TrainConfig.IMAGE_MAX_OUTPUT))
                if TrainConfig.IMAGE_PRE_NMS:
                    pre_nms_placeholder = tf.placeholder(
                        tf.float32,
                        shape=[TrainConfig.IMAGE_MAX_OUTPUT, *data_shape],
                        name='pre_nms_placeholder')
                    image_summaries.append(tf.summary.image(
                        'pre_nms', pre_nms_placeholder, max_outputs=TrainConfig.IMAGE_MAX_OUTPUT))
                post_nms_placeholder = tf.placeholder(
                    tf.float32,
                    shape=[TrainConfig.IMAGE_MAX_OUTPUT, *data_shape],
                    name='post_nms_placeholder')
                image_summaries.append(tf.summary.image(
                    'post_nms', post_nms_placeholder, max_outputs=TrainConfig.IMAGE_MAX_OUTPUT))
            with tf.variable_scope('Anchor'):
                detector_anchor_placeholders = []
                detector_anchors = []
                for detector in ssd_network.detectors:
                    logger.debug('Detector anchors : {} => {} x {} boxes = {}'.format(
                        detector.shape[1:3],
                        len(SSDConfig.LAYER_RATIOS[0]), detector.shape[1] * detector.shape[2],
                        len(SSDConfig.LAYER_RATIOS[0]) * detector.shape[1] * detector.shape[2],))
                    detector_anchors.append(calculate_anchors(
                        data_shape, detector.shape.as_list()[1:3], SSDConfig.LAYER_RATIOS[0]))
                    if TrainConfig.IMAGE_ANCHORS:
                        anchor_placeholder = tf.placeholder(
                            tf.float32,
                            shape=[TrainConfig.IMAGE_MAX_OUTPUT, *data_shape],
                            name=detector.name.split('/')[3] + '_placeholder')
                        detector_anchor_placeholders.append(anchor_placeholder)
                        anchor_summaries.append(tf.summary.image(
                            detector.name.split('/')[3], anchor_placeholder, max_outputs=TrainConfig.IMAGE_MAX_OUTPUT))

        total_anchors = np.concatenate(detector_anchors, 0)
        # -------- Train and Evaluate the Mode --------
        with tf.variable_scope('Train'):
            ratio_count = len(SSDConfig.LAYER_RATIOS[0])
            losses = []
            detector_labels = []
            for detector_index, detector in enumerate(ssd_network.detectors):
                detector_shape = detector.shape.as_list()
                anchor_count = detector_shape[1] * detector_shape[2]
                detector_output_op = tf.reshape(
                    detector, [-1, ratio_count, anchor_count, SSDConfig.LOCATION_DIMMENSION + SSDConfig.OUTPUT_CLASSES])
                with tf.variable_scope(detector.name.split('/')[2]):
                    mask_placeholder = tf.placeholder(
                        tf.float32,
                        shape=[
                            None, ratio_count, anchor_count, SSDConfig.LOCATION_DIMMENSION + SSDConfig.OUTPUT_CLASSES],
                        name='label_placeholder')
                    label_placeholder = tf.placeholder(
                        tf.float32,
                        shape=[
                            None, ratio_count, anchor_count, SSDConfig.LOCATION_DIMMENSION + SSDConfig.OUTPUT_CLASSES],
                        name='label_placeholder')
                    detector_labels.append(label_placeholder)
                    with tf.variable_scope('loss'):
                        masked_output_op = tf.multiply(detector_output_op, mask_placeholder)
                        loss_loc_op = tf.reduce_mean(tf.pow(
                            label_placeholder[:, :, :, :5] - masked_output_op[:, :, :, :5], 2), axis=-1)
                        loss_class_op = tf.nn.softmax_cross_entropy_with_logits_v2(
                            labels=label_placeholder[:, :, :, 5:],
                            logits=masked_output_op[:, :, :, 5:])
                        reduced_loss_op = tf.reduce_mean(loss_loc_op) + tf.reduce_mean(loss_class_op)
                        losses.append(reduced_loss_op)
            with tf.variable_scope('loss'):
                # loss_op = tf.stack(losses, axis=-1)
                # reduced_loss_op = tf.reduce_mean(loss_op)
                # train_summaries.append(tf.summary.scalar('loss', reduced_loss_op))
                loss_op = tf.constant(0)
                reduced_loss_op = loss_op

            def minimize_with_summaries(optimizer, loss, var_list=None, global_step=None):
                gradients = optimizer.compute_gradients(loss, var_list=var_list)
                train_op = optimizer.apply_gradients(gradients, global_step=global_step)
                # Add histograms for all gradients for every layer.
                summaries = []
                for grad, var in gradients:
                    if grad is not None:
                        summaries.append(tf.summary.histogram(var.op.name + "/gradients", grad))
                return train_op, summaries

            # with tf.variable_scope("optimizer"):
                # update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
                # step_op = tf.Variable(0, trainable=False, name="Step")
                # learning_rate_op = tf.train.exponential_decay(TrainConfig.LEARNING_RATE, step_op, 10, 0.95)
                # train_summaries.append(tf.summary.scalar('learning_rate', learning_rate_op))
                # momentum_op = 0.6 - tf.train.exponential_decay(0.6, step_op, 200, 0.92)
                # train_summaries.append(tf.summary.scalar('momentum', momentum_op))
                # with tf.control_dependencies(update_ops):
                    # train_op = tf.train.MomentumOptimizer(
                    #     learning_rate_op, 0.6).minimize(loss_op, global_step=step_op)
                    # train_op, optimizer_summaries = minimize_with_summaries(
                    #     tf.train.AdamOptimizer(TrainConfig.LEARNING_RATE), loss_op)
                    # train_op = tf.train.AdamOptimizer(TrainConfig.LEARNING_RATE).minimize(loss_op)
                    # train_summaries.extend(optimizer_summaries)

            with tf.variable_scope('Accuracy'):
                accuracy_op = tf.constant(0)
                # accuracy_op = 1 - tf.reduce_mean(
                #     tf.abs(processed_label_op - output_op))
                train_summaries.append(tf.summary.scalar('accuracy', accuracy_op))

        session.run(tf.global_variables_initializer())
        session.run(tf.local_variables_initializer())

        # -------- Training loop --------
        train_data_size = len(train_data)
        step_per_epoch = int(train_data_size / TrainConfig.BATCH_SIZE)
        if (step_per_epoch * TrainConfig.BATCH_SIZE) < train_data_size:
            step_per_epoch += 1
        checkpoints = []
        min_checkpoint_accuracy = 0

        if args.output_dir:
            if os.path.exists(args.output_dir):
                shutil.rmtree(args.output_dir)
            os.makedirs(args.output_dir)

            # Saving the configuration as JSON files
            # summary_op = tf.summary.merge(
            #     train_summaries + [layer.summaries.histograms for layer in network.layers])
            summary_op = tf.summary.merge(train_summaries)
            image_op = tf.summary.merge(image_summaries)
            if TrainConfig.IMAGE_ANCHORS:
                anchor_summary_op = tf.summary.merge(anchor_summaries)
            train_writer = tf.summary.FileWriter(
                os.path.join(args.output_dir, "train"), session.graph, flush_secs=30)
            # tf.all_variables()
            # tf.global_variables(scope="Network")
            # tf.trainable_variables(scope="Network")
            saver = tf.train.Saver(tf.global_variables(scope="Network"), max_to_keep=100)
        else:
            train_writer = None

        logger.info('Training... ({} samples, {} steps per epoch)'.format(train_data_size, step_per_epoch))
        summary_period = int(step_per_epoch / TrainConfig.SUMMARIES_PER_EPOCH)
        if summary_period == 0:
            summary_period = 1
        image_period = int(step_per_epoch / TrainConfig.IMAGES_PER_EPOCH)
        if image_period == 0:
            image_period = 1
        train_start_time = time.time()
        benchmark_time = time.time()
        benchmark_step = 0
        index_list = np.arange(train_data_size)

        if TrainConfig.IMAGE_ANCHORS:
            feed_dict = {}
            for detector_index, detector_placeholder in enumerate(detector_anchor_placeholders):
                feed_dict[detector_placeholder] = render_anchors_box(
                    np.zeros([TrainConfig.IMAGE_MAX_OUTPUT, *data_shape], float),
                    detector_anchors[detector_index])
            anchor_summary = session.run(
                anchor_summary_op,
                feed_dict=feed_dict)
            train_writer.add_summary(anchor_summary, global_step=0)

        try:
            # from tensorflow.python.client import timeline
            global_step = 0
            batch_norm_train = True
            batch_data = []
            batch_labels = []

            def save_summary(min_accuracy):
                if train_writer:
                    result, train_loss, train_accuracy, summary = session.run(
                        [network_output_op, reduced_loss_op, accuracy_op, summary_op],
                        feed_dict={
                            data_placeholder: batch_data,
                            # label_placeholder: batch_labels,
                            batch_norm_placeholder: False})
                    train_writer.add_summary(summary, global_step=global_step)
                    # train_writer.flush()
                else:
                    train_loss, train_accuracy = session.run(
                        [reduced_loss_op, accuracy_op],
                        feed_dict={
                            data_placeholder: batch_data,
                            # label_placeholder: batch_labels,
                            batch_norm_placeholder: False})
                if train_accuracy > min_accuracy:
                    checkpoint_name = "checkpoint_acc_%0.03f" % train_accuracy
                    if len(checkpoints) > TrainConfig.MAX_CHECKPOINT:
                        removing_checkpoint = checkpoints.pop()
                        checkpoint_folder_content = os.listdir(args.output_dir)
                        for entry in checkpoint_folder_content:
                            if entry.startswith(removing_checkpoint["name"]):
                                os.remove(os.path.join(args.output_dir, entry))
                    checkpoints.append({
                        "acc": train_accuracy,
                        "name": checkpoint_name
                    })
                    saver.save(session, os.path.join(args.output_dir, checkpoint_name), global_step=global_step)
                    checkpoints.sort(key=lambda x: x["acc"], reverse=True)
                    min_accuracy = checkpoints[-1]["acc"]

                speed = benchmark_step / (time.time() - benchmark_time)
                logger.info(
                    'Global step %d, training_loss: %.03Ef, training acc %.03f, %0.02f steps/s, %0.02f input/sec' % (
                        global_step, train_loss, train_accuracy, speed, speed * TrainConfig.BATCH_SIZE))
                return min_accuracy

            if saver:
                saver.save(session, os.path.join(args.output_dir, "checkpoint_0"))
            for epoch in range(TrainConfig.TRAIN_EPOCH):
                np.random.shuffle(index_list)
                batch_iterator = 0
                if epoch == TrainConfig.TRAIN_EPOCH / 2:
                    logger.info('Switching batch norm training to %s     ' % str(not batch_norm_train))
                    batch_norm_train = not batch_norm_train
                print('Epoch %d                                            ' % epoch)
                for step in range(step_per_epoch):
                    if benchmark_step > 1:
                        speed = benchmark_step / (time.time() - benchmark_time)
                        print("Step %d, %0.02f steps/s, %0.02f input/sec     " % (
                            step, speed, speed * TrainConfig.BATCH_SIZE), end="\r")
                    if (batch_iterator + TrainConfig.BATCH_SIZE) > train_data_size:
                        batch_data = [train_data[index] for index in index_list[batch_iterator:train_data_size]]
                        batch_labels = [train_labels[index] for index in index_list[batch_iterator:train_data_size]]
                    else:
                        batch_data = [train_data[index] for index in index_list[
                            batch_iterator:batch_iterator + TrainConfig.BATCH_SIZE]]
                        batch_labels = [train_labels[index] for index in index_list[
                            batch_iterator:batch_iterator + TrainConfig.BATCH_SIZE]]
                    # for index, label in enumerate(batch_labels):
                    #     if len(label) < TrainConfig.MAX_BOX_PER_IMAGE:
                    #         batch_labels[index] = np.concatenate([label, np.zeros(
                    #             (TrainConfig.MAX_BOX_PER_IMAGE - len(label), label_dimension), dtype=np.int)])
                    predictions = session.run(
                        network_output_op,
                        feed_dict={
                            data_placeholder: batch_data,
                            # label_placeholder: batch_labels,
                            batch_norm_placeholder: batch_norm_train})
                    raw_boxes = []
                    total_boxes = []
                    for image_index, image in enumerate(batch_data[:TrainConfig.IMAGE_MAX_OUTPUT]):
                        class_boxes = []
                        raw_class_boxes = []
                        for class_index in range(SSDConfig.OUTPUT_CLASSES):
                            boxes = get_boxes(
                                predictions[image_index], total_anchors, class_index + SSDConfig.LOCATION_DIMMENSION)
                            boxes = np.asarray([box for box in boxes if box[-1] > SSDConfig.CONFIDENCE_THRESHOLD])
                            # logger.debug('Pre NMS boxes : {}'.format(boxes.shape))
                            raw_class_boxes.append(boxes)
                            nms_boxes = fast_nms(boxes, SSDConfig.NMS_IOU)
                            # logger.debug('Post NMS boxes : {}'.format(nms_boxes.shape))
                            class_boxes.append(nms_boxes)
                        raw_boxes.append(raw_class_boxes)
                        total_boxes.append(class_boxes)
                    if epoch > TrainConfig.SKIP_FIRST_EPOCHS - 1:
                        if (global_step % summary_period) == (summary_period - 1):
                            min_checkpoint_accuracy = save_summary(min_checkpoint_accuracy)
                            benchmark_time = time.time()
                            benchmark_step = 0
                        if (global_step % image_period) == (image_period - 1):
                            feed_dict = {
                                data_placeholder: batch_data,
                                # label_placeholder: batch_labels,
                                post_nms_placeholder: render_box(
                                    np.copy(batch_data[:TrainConfig.IMAGE_MAX_OUTPUT]), total_boxes[0]),
                                label_image_placeholder: render_bounding_box(
                                    np.copy(batch_data[:TrainConfig.IMAGE_MAX_OUTPUT]), batch_labels),
                                batch_norm_placeholder: batch_norm_train}
                            if TrainConfig.IMAGE_PRE_NMS:
                                feed_dict[pre_nms_placeholder] = render_box(
                                    np.copy(batch_data[:TrainConfig.IMAGE_MAX_OUTPUT]), raw_boxes[0]),
                            images = session.run(
                                image_op,
                                feed_dict=feed_dict)
                            train_writer.add_summary(images, global_step=global_step)
                            exit()
                    global_step += 1
                    benchmark_step += 1
                    batch_iterator += TrainConfig.BATCH_SIZE
        except KeyboardInterrupt:
            pass
        save_summary(min_checkpoint_accuracy)

        train_stop_time = time.time()

        train_memory_peak = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
        train_gpu_memory = subprocess.check_output(
            "nvidia-smi --query-gpu=memory.used --format=csv,noheader", shell=True).decode()

        if "CUDA_VISIBLE_DEVICES" in os.environ:
            train_gpu_memory = train_gpu_memory.split("\n")[int(os.environ["CUDA_VISIBLE_DEVICES"])]
        else:
            train_gpu_memory = " ".join(train_gpu_memory.split("\n"))

        result = "Training time : %gs\n\tRAM peak : %g MB\n\tVRAM usage : %s" % (
            train_stop_time - train_start_time,
            int(train_memory_peak / 1024),
            train_gpu_memory)
        logger.info(result)
