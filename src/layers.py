import tensorflow as tf
from tensorflow.python.training import moving_averages

from src.utils.logger import DummyLogger


class LayerInfo():
    def __init__(self):
        self.memory = 0.0
        self.ops = 0.0
        self.output = 0.0


class LayerSummaries():
    def __init__(self):
        self.scalars = []
        self.histograms = []
        self.images = []


class Layer():
    # Default layer arguments
    ACTIVATION = tf.nn.relu
    BATCH_NORM_TRAINING = False
    IS_TRAINING = False
    METRICS = False
    VERBOSE = 0
    LOGGER = DummyLogger()

    def __init__(self):
        self.input_op = None
        self.output_op = None
        self.name = 'Layer'

        self.info = LayerInfo()
        self.summaries = LayerSummaries()

    def finalize(self, output, activation=0, batch_norm=True,
                 is_training=False, batch_norm_training=False):
        if activation == 0:
            activation = Layer.ACTIVATION
        if activation:
            output = activation(output)
            if Layer.METRICS:
                self.summaries.histograms.append(tf.summary.histogram('Activation', output))

        if batch_norm:
            output = BatchNormalization(
                output, is_training=is_training,
                batch_norm_training=batch_norm_training, name='Batch_Norm').output_op
            if Layer.METRICS:
                self.summaries.histograms.append(tf.summary.histogram('PostBatchNorm', output))
        self.output_op = output


class BatchNormalization(Layer):

    def __init__(self, input_op, is_training=None, batch_norm_training=None,
                 epsilon=0.001, decay=0.999, name=None):
        # Perform a batch normalization after a conv layer or a fc layer
        # gamma: a scale factor
        # beta: an offset
        # epsilon: the variance epsilon - a small float number to avoid dividing by 0
        if is_training is None:
            is_training = Layer.IS_TRAINING
        if batch_norm_training is None:
            batch_norm_training = Layer.BATCH_NORM_TRAINING

        self.input_op = input_op

        inputs_shape = input_op.get_shape()
        axis = list(range(len(inputs_shape) - 1))
        params_shape = inputs_shape[-1:]

        gamma_init = tf.ones_initializer()
        beta_init = tf.zeros_initializer()

        moving_mean_init = tf.zeros_initializer()
        moving_variance_init = tf.ones_initializer()

        with tf.variable_scope(None, default_name=name) as layer_scope:
            self.name = layer_scope.name
            gamma = tf.get_variable('gamma', shape=params_shape, initializer=gamma_init)
            beta = tf.get_variable('beta', shape=params_shape, initializer=beta_init)

            moving_mean = tf.get_variable('moving_mean', shape=params_shape,
                                          initializer=moving_mean_init, trainable=is_training)
            moving_variance = tf.get_variable('moving_var', shape=params_shape,
                                              initializer=moving_variance_init, trainable=is_training)

            if is_training:
                # Calculate the moments based on the individual batch.
                mean, variance = tf.nn.moments(input_op, axis)

                update_moving_mean = moving_averages.assign_moving_average(
                    moving_mean, mean, decay)
                update_moving_variance = moving_averages.assign_moving_average(
                    moving_variance, variance, decay)
                control_inputs = [update_moving_mean, update_moving_variance]

                def training():
                    return tf.nn.batch_normalization(
                        input_op, mean, variance, offset=beta, scale=gamma,
                        variance_epsilon=epsilon, name='batch_norm')

                def non_training():
                    return tf.nn.batch_normalization(
                        input_op, moving_mean, moving_variance, offset=beta, scale=gamma,
                        variance_epsilon=epsilon, name='batch_norm_no_train')

                with tf.control_dependencies(control_inputs):
                    output = tf.cond(batch_norm_training, training, non_training)
            else:
                # Just use the moving_mean and moving_variance.
                output = tf.nn.batch_normalization(
                    input_op, moving_mean, moving_variance, offset=beta, scale=gamma,
                    variance_epsilon=epsilon, name='batch_norm')

            self.output_op = output


class Conv1D(Layer):

    def __init__(self, input_op, output_features, name="Conv", is_training=None,
                 kernel_size=3, stride=1, activation=0,
                 batch_norm=True, batch_norm_training=None):
        if is_training is None:
            is_training = Layer.IS_TRAINING
        if batch_norm_training is None:
            batch_norm_training = Layer.BATCH_NORM_TRAINING
        super().__init__()
        self.input_op = input_op
        self.name = name
        input_shape = input_op.get_shape().as_list()
        input_features = input_shape[-1]
        self.info.memory += 4 * kernel_size * input_features * output_features  # 32bits = 4 bytes
        self.info.output = 4 * (input_shape[1] / stride) * output_features

        with tf.variable_scope(None, default_name=name):
            weights = tf.Variable(
                tf.truncated_normal(
                    [kernel_size, input_features, output_features], stddev=0.1),
                name="Kernel") if is_training else (
                    tf.get_variable('Kernel', shape=[kernel_size, input_features, output_features],
                                    initializer=tf.zeros_initializer())
                )
            biases = tf.Variable(tf.constant(0.1, shape=[output_features]), name="Bias")
            output = tf.nn.conv1d(input_op, weights, stride, padding='SAME') + biases
            if Layer.VERBOSE:
                Layer.LOGGER.info("Conv1D %s : %s => %s (memory : %.02fKiB, output : %.02fKiB)" % (
                    name, str(input_op.shape), str(output.shape),
                    self.info.memory / 1024, self.info.output / 1024))
            if Layer.METRICS:
                self.summaries.histograms.append(tf.summary.histogram('Kernel', weights))
                self.summaries.histograms.append(tf.summary.histogram('Biases', biases))
                self.summaries.histograms.append(tf.summary.histogram('Output', output))

            self.finalize(output, activation=activation, batch_norm=batch_norm,
                          is_training=is_training, batch_norm_training=batch_norm_training)


class DeConv1D(Layer):
    def __init__(self, input_op, output_features=None, output_shape=None, name="DeConv",
                 is_training=None, kernel_size=3, stride=1, activation=0,
                 batch_norm=True, batch_norm_training=None):
        if is_training is None:
            is_training = Layer.IS_TRAINING
        if batch_norm_training is None:
            batch_norm_training = Layer.BATCH_NORM_TRAINING
        super().__init__()
        self.input_op = input_op
        self.name = name
        input_shape = input_op.get_shape().as_list()
        input_features = input_shape[-1]
        self.info.memory += 4 * kernel_size * input_features * output_features  # 32bits = 4 bytes
        self.info.output = 4 * (input_shape[1] * stride) * output_features

        with tf.variable_scope(None, default_name=name):
            if not output_shape:
                output_shape = input_op.get_shape().as_list()
                output_shape[0] = tf.shape(input_op)[0]
                output_shape[1] *= stride
                output_shape[2] = output_features
            else:
                output_shape[0] = tf.shape(input_op)[0]
                if output_features is None:
                    output_features = output_shape[3]
                else:
                    output_shape[3] = output_features
            weights = tf.Variable(
                tf.truncated_normal(
                    [kernel_size, output_features, input_features], stddev=0.1),
                name="Kernel") if is_training else (
                    tf.get_variable('Kernel', shape=[kernel_size, output_features, input_features],
                                    initializer=tf.zeros_initializer())
                )
            biases = tf.Variable(tf.constant(0.1, shape=[output_features]), name="Bias")
            output = tf.contrib.nn.conv1d_transpose(input_op, weights, output_shape, stride, padding='SAME') + biases
            if Layer.VERBOSE:
                Layer.LOGGER.info("DeConv1D %s : %s => %s (memory : %.02fKiB, output : %.02fKiB)" % (
                    name, str(input_op.shape), str(output.shape),
                    self.info.memory / 1024, self.info.output / 1024))
            if Layer.METRICS:
                self.summaries.histograms.append(tf.summary.histogram('Weights', weights))
                self.summaries.histograms.append(tf.summary.histogram('Biases', biases))
                self.summaries.histograms.append(tf.summary.histogram('Output', output))

            self.finalize(output, activation=activation, batch_norm=batch_norm,
                          is_training=is_training, batch_norm_training=batch_norm_training)


class Conv2D(Layer):

    def __init__(self, input_op, output_features, name="Conv", is_training=None,
                 kernel_size=3, strides=[1, 1, 1, 1], activation=0, batch_norm=True,
                 batch_norm_training=None):
        if is_training is None:
            is_training = Layer.IS_TRAINING
        if batch_norm_training is None:
            batch_norm_training = Layer.BATCH_NORM_TRAINING
        super().__init__()
        self.input_op = input_op
        input_shape = input_op.get_shape().as_list()
        input_features = input_shape[-1]
        self.info.memory += 4 * kernel_size * kernel_size * input_features * output_features  # 32bits = 4 bytes
        self.info.output = 4 * (input_shape[1] / strides[1]) * (input_shape[2] / strides[2]) * output_features

        with tf.variable_scope(None, default_name=name) as layer_scope:
            self.name = layer_scope.name
            weights = tf.Variable(
                tf.truncated_normal(
                    [kernel_size, kernel_size, input_features, output_features], stddev=0.1),
                name="Kernel") if is_training else (
                    tf.get_variable('Kernel', shape=[kernel_size, kernel_size, input_features, output_features],
                                    initializer=tf.zeros_initializer())
                )
            biases = tf.Variable(tf.constant(0.1, shape=[output_features]), name="Bias")
            output = tf.nn.conv2d(input_op, weights, strides=strides, padding='SAME') + biases
            if Layer.VERBOSE:
                Layer.LOGGER.info("Conv2D %s (%dx%d) : %s => %s (memory : %.02fKiB, output : %.02fKiB)" % (
                    self.name.split('/')[-1], kernel_size, kernel_size, str(input_op.shape), str(output.shape),
                    self.info.memory / 1024, self.info.output / 1024))
            if Layer.METRICS:
                self.summaries.histograms.append(tf.summary.histogram('Kernel', weights))
                self.summaries.histograms.append(tf.summary.histogram('Biases', biases))
                self.summaries.histograms.append(tf.summary.histogram('Output', output))

            self.finalize(output, activation=activation, batch_norm=batch_norm,
                          is_training=is_training, batch_norm_training=batch_norm_training)


class DeConv2D(Layer):
    def __init__(self, input_op, output_features=None, output_shape=None, name="DeConv",
                 is_training=None, kernel_size=3, strides=[1, 1, 1, 1], activation=0,
                 batch_norm=True, batch_norm_training=None):
        if is_training is None:
            is_training = Layer.IS_TRAINING
        if batch_norm_training is None:
            batch_norm_training = Layer.BATCH_NORM_TRAINING
        super().__init__()
        self.input_op = input_op
        self.name = name
        input_shape = input_op.get_shape().as_list()
        input_features = input_shape[-1]

        with tf.variable_scope(None, default_name=name):
            if not output_shape:
                output_shape = input_op.get_shape().as_list()  # [?, 32, 32, output_features]
                output_shape[0] = tf.shape(input_op)[0]
                output_shape[1] *= strides[1]
                output_shape[2] *= strides[2]
                output_shape[3] = output_features
            else:
                output_shape[0] = tf.shape(input_op)[0]
                if output_features is None:
                    output_features = output_shape[3]
                else:
                    output_shape[3] = output_features
            self.info.memory += 4 * kernel_size * kernel_size * input_features * output_features  # 32bits = 4 bytes
            self.info.output = 4 * output_shape[1] * output_shape[2] * output_features
            weights = tf.Variable(
                tf.truncated_normal(
                    [kernel_size, kernel_size, output_features, input_features], stddev=0.1),
                name="Kernel") if is_training else (
                    tf.get_variable('Kernel', shape=[kernel_size, kernel_size, output_features, input_features],
                                    initializer=tf.zeros_initializer())
                )
            biases = tf.Variable(tf.constant(0.1, shape=[output_features]), name="Bias")
            output = tf.nn.conv2d_transpose(input_op, weights, output_shape, strides, padding='SAME') + biases
            if Layer.VERBOSE:
                Layer.LOGGER.info("DeConv2D %s (%dx%d) : %s => %s (memory : %.02fKiB, output : %.02fKiB)" % (
                    name, kernel_size, kernel_size, str(input_op.shape), str(output.shape),
                    self.info.memory / 1024, self.info.output / 1024))
            if Layer.METRICS:
                self.summaries.histograms.append(tf.summary.histogram('Weights', weights))
                self.summaries.histograms.append(tf.summary.histogram('Biases', biases))
                self.summaries.histograms.append(tf.summary.histogram('Output', output))

            self.finalize(output, activation=activation, batch_norm=batch_norm,
                          is_training=is_training, batch_norm_training=batch_norm_training)


class FullyConnected(Layer):

    def __init__(self, input_op, output_dimmension, name="FC", is_training=None,
                 activation=0, batch_norm=True, batch_norm_training=None):
        if is_training is None:
            is_training = Layer.IS_TRAINING
        if batch_norm_training is None:
            batch_norm_training = Layer.BATCH_NORM_TRAINING
        super().__init__()
        self.input_op = input_op
        self.name = name
        input_dimmension = input_op.get_shape().as_list()[-1]
        self.info.memory = 4 * input_dimmension * output_dimmension  # 32bits = 4 bytes
        self.info.output = 4 * output_dimmension  # 32bits = 4 bytes
        with tf.variable_scope(None, default_name=name):
            weights = tf.Variable(
                tf.truncated_normal(
                    [input_dimmension, output_dimmension], stddev=0.1),
                name="Weight") if is_training else (
                    tf.get_variable('Weight', shape=[input_dimmension, output_dimmension],
                                    initializer=tf.zeros_initializer())
                )
            biases = tf.Variable(tf.constant(0.1, shape=[output_dimmension]), name="Bias")
            output = tf.matmul(input_op, weights) + biases
            if Layer.VERBOSE:
                Layer.LOGGER.info("Conv1D %s : : %s => %s (memory : %.02fKiB, output : &.02fKiB)" % (
                    name, str(input_op.shape), str(output.shape),
                    self.info.memory / 1024, self.info.output / 1024))
            if Layer.METRICS:
                self.summaries.histograms.append(tf.summary.histogram('Weights', weights))
                self.summaries.histograms.append(tf.summary.histogram('Biases', biases))
                self.summaries.histograms.append(tf.summary.histogram('Output', output))

            self.finalize(output, activation=activation, batch_norm=batch_norm,
                          is_training=is_training, batch_norm_training=batch_norm_training)


class LSTM(Layer):

    def __init__(self, output_dimmension, name="LSTM", activation=0):
        self.name = name
        # input_dimmension = input_op.shape[-1]
        # self.info.memory = 4 * input_dimmension * ouput_dimmension  # 32bits = 4 bytes

        with tf.variable_scope(None, default_name=name):
            self.output_op = tf.contrib.rnn.BasicLSTMCell(output_dimmension, activation=activation)
