import multiprocessing as mp
import os
import xml.etree.ElementTree as ET

from PIL import Image, ImageDraw
import numpy as np

from src.utils.logger import DummyLogger


class PascalVoc2012():
    CLASSES = ['aeroplane', 'bicycle', 'bird', 'boat', 'bottle',
               'bus', 'car', 'cat', 'chair', 'cow',
               'diningtable', 'dog', 'horse', 'motorbike', 'person',
               'pottedplant', 'sheep', 'sofa', 'train', 'tvmonitor']
    DATASET_DIR = None
    ANNOTATION_DIR = None
    WORKER_ARGUMENTS = None

    @staticmethod
    def load_data(dataset_path, class_list=None, data_limit=None, resize=None,
                  allow_truncated=True, allow_occluded=False, allow_difficult=False, min_size=None,
                  logger=DummyLogger()):
        if class_list is None:
            class_list = PascalVoc2012.CLASSES
        annotation_dir = os.path.join(dataset_path, 'Annotations')

        label_files = os.listdir(annotation_dir)
        if data_limit:
            label_files = label_files[:data_limit]
        PascalVoc2012.DATASET_DIR = dataset_path
        PascalVoc2012.ANNOTATION_DIR = annotation_dir
        PascalVoc2012.WORKER_ARGUMENTS = [
            class_list, resize, min_size, allow_truncated, allow_occluded, allow_difficult, logger]

        pool_size = (mp.cpu_count() // 2) + 1
        pool_arguments = []
        data_per_worker = len(label_files) // pool_size
        for pool_index in range(pool_size - 1):
            pool_arguments.append(label_files[data_per_worker * pool_index:data_per_worker * (pool_index + 1)])
        pool_arguments.append(label_files[data_per_worker * (pool_size - 1):])

        logger.info('Loading images')
        pool = mp.Pool(processes=pool_size)
        data = np.asarray(pool.map(PascalVoc2012._load_file, pool_arguments))
        images = np.concatenate(data[:, 0])
        labels = np.concatenate(data[:, 1])
        return images, labels

    @staticmethod
    def _load_file(entry_list):
        (class_list, resize, min_size, allow_truncated,
         allow_occluded, allow_difficult, logger) = PascalVoc2012.WORKER_ARGUMENTS
        images = []
        labels = []

        for entry in entry_list:
            tree = ET.parse(os.path.join(PascalVoc2012.ANNOTATION_DIR, entry))
            root = tree.getroot()
            image_labels = []
            for object_element in root.iter('object'):
                # Check image size
                if min_size:
                    object_size = root.find('size')
                    if int(object_size.find('width').text) < min_size and int(
                            object_size.find('width').text) < min_size:
                        continue
                object_class = object_element.find('name').text
                # Check object class
                if object_class in class_list:
                    # Efficently parse object (avoid multiple find)
                    elements = list(object_element)
                    truncated = False
                    occluded = False
                    difficult = False
                    object_bb = None
                    for element in elements:
                        if element.tag == 'truncated':
                            truncated = element.text != '0'
                        if element.tag == 'occluded':
                            occluded = element.text != '0'
                        if element.tag == 'difficult':
                            difficult = element.text != '0'
                        if element.tag == 'bndbox':
                            object_bb = element
                    # Check object is allowed
                    if not allow_truncated and truncated:
                        continue
                    if not allow_occluded and occluded:
                        continue
                    if not allow_difficult and difficult:
                        continue

                    # Add current object to entry_labels
                    try:
                        x_min = int(object_bb.find('xmin').text) - 1
                        y_min = int(object_bb.find('ymin').text) - 1
                        x_max = int(object_bb.find('xmax').text) - 1
                        y_max = int(object_bb.find('ymax').text) - 1
                    except ValueError as error:
                        logger.warning('Ignoring object {} of file {} :\n{}'.format(
                            object_class, root.find('filename').text, error))
                        continue
                    image_labels.append(np.array([
                        x_min,
                        y_min,
                        x_max - x_min,
                        y_max - y_min,
                        *[int(class_name == object_class) for class_name in class_list]
                    ]))

            if image_labels:
                # Add image and labels to result list
                object_file = root.find('filename').text
                image, label = PascalVoc2012.get_image_label(
                    os.path.join(PascalVoc2012.DATASET_DIR, 'JPEGImages', object_file),
                    image_labels,
                    resize=resize)
                images.append(image)
                # if len(label) < 10:
                #     label = np.concatenate([label, np.zeros(
                #         (10 - len(label), label[0].shape[-1]), dtype=np.int)])
                labels.append(label)
        return np.asarray(images), np.asarray(labels)

    @staticmethod
    def get_image_label(image_path, image_labels, resize=None):
        # test_path = 'test'
        if resize is not None:
            image = Image.open(image_path)
            # if test_path:
            #     test_image = image.copy()
            #     draw = ImageDraw.Draw(test_image)
            #     for label in image_labels:
            #         if label[2] != 0:
            #             draw.rectangle(
            #                 (label[0], label[1], label[0] + label[2], label[1] + label[3]),
            #                 outline=(255, 0, 0))
            #     test_image.save(os.path.join(test_path, os.path.basename(image_path) + '_ori'), 'PNG')
            if image.size[0] != resize[0] or image.size[1] != resize[1]:
                image.thumbnail(resize)
                x_padding = (resize[0] - image.size[0]) // 2
                y_padding = (resize[1] - image.size[1]) // 2
                if x_padding:
                    for label in image_labels:
                        label[0] += x_padding
                if y_padding:
                    for label in image_labels:
                        label[1] += y_padding
                resized_image = Image.new('RGB', resize, (0, 0, 0,))
                resized_image.paste(
                    image,
                    ((resize[0] - image.size[0]) // 2, (resize[1] - image.size[1]) // 2))
            else:
                resized_image = image
            # if test_path:
            #     test_image = resized_image.copy()
            #     draw = ImageDraw.Draw(test_image)
            #     for label in image_labels:
            #         if label[2] != 0:
            #             draw.rectangle(
            #                 (label[0], label[1], label[0] + label[2], label[1] + label[3]),
            #                 outline=(255, 0, 0))
            #     test_image.save(os.path.join(test_path, os.path.basename(image_path)), 'PNG')
            return np.asarray(resized_image), np.asarray(image_labels)
        return np.asarray(Image.open(image_path)), np.asarray(image_labels)
