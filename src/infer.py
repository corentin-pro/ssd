import math
import os

import numpy as np
from PIL import Image

import src.layers as layers
from src.model import Network


def infer(input_files, batch_size, checkpoint_path, output_dir=None, testing_output=False):
    input_data = []
    for file_path in input_files:
        image_array = np.asarray(Image.open(file_path).convert('RGB'))
        input_data.append(image_array)
    input_data = np.asarray(input_data)
    if testing_output:
        print('Running inference on test data : %s' % str(input_data.shape))

    import tensorflow as tf
    # manage memory(True->if we need then use, False->use all.This is same as default)
    gpu_options = tf.GPUOptions(allow_growth=True)
    session_config = tf.ConfigProto(gpu_options=gpu_options)

    with tf.Session(config=session_config).as_default() as session:
        # Setting the placeholders
        data_placeholder_op = tf.placeholder(
            tf.float32,
            shape=[batch_size, *input_data.shape[1:]],
            name='data_placeholder')

        with tf.variable_scope('Pre_Process'):
            processed_data_op = data_placeholder_op / 255

        with tf.variable_scope('Network'):
            layers.Layer.ACTIVATION = tf.nn.leaky_relu
            network = Network(processed_data_op)

        with tf.variable_scope('Post_Process'):
            processed_output_op = tf.squeeze(network.output_op * 255, axis=-1)

        # session.run(tf.global_variables_initializer())
        # session.run(tf.local_variables_initializer())
        saver = tf.train.Saver()
        saver.restore(session, checkpoint_path)
        if testing_output:
            print('Checkpoint restored')

        step_count = math.ceil(input_data.shape[0] / batch_size)
        current_index = 0
        infer_output = []
        if output_dir:
            os.makedirs(output_dir, exist_ok=True)
            if testing_output:
                os.makedirs(os.path.join(output_dir, 'concatenated'), exist_ok=True)
        if testing_output:
            print('Number of testing test : %d (batch size : %d)' % (step_count, batch_size))
        for step in range(step_count):
            if testing_output:
                print('Step %d' % step, end='\r')
            if (current_index + batch_size) > input_data.shape[0]:
                batch_data = input_data[current_index:]
            else:
                batch_data = input_data[current_index:current_index + batch_size]
            network_output = session.run(
                processed_output_op,
                feed_dict={data_placeholder_op: batch_data})
            if not output_dir:
                infer_output.append(network_output)
            else:
                for image_index in range(len(network_output)):
                    result_image = Image.fromarray(network_output[image_index].astype(np.uint8), 'L')
                    result_image.save(
                        os.path.join(output_dir, os.path.basename(input_files[current_index + image_index])))
                    if testing_output:
                        input_image = Image.fromarray(input_data[current_index + image_index].astype(np.uint8), 'RGB')
                        label_image = Image.open(
                            input_files[current_index + image_index][:-4] + '_Depth.png')
                        concat_image = Image.new('RGB', (3 * network_output.shape[1], network_output.shape[2]))
                        concat_image.paste(input_image, (0, 0))
                        concat_image.paste(label_image, (network_output.shape[1], 0))
                        concat_image.paste(result_image.convert('RGB'), (2 * network_output.shape[1], 0))
                        concat_image.save(
                            os.path.join(
                                output_dir,
                                'concatenated',
                                os.path.basename(input_files[current_index + image_index])))
            current_index += batch_size

        if testing_output:
            print('\nTest done')
        if not output_dir:
            return np.asarray(infer_output)
