import argparse
import os
import sys
import traceback

import numpy as np
import h5py

from src.train import train
from src.utils.logger import create_logger


def main():
    global POOL_ARGUMENTS
    global POOL_LOADER
    logger = create_logger('train_ssd', 'log', True)
    logger.info('-------- Starting train --------')
    logger.info('From command : ' + ' '.join(sys.argv))

    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('dataset', help='Name of dataset to train')
        parser.add_argument('--class_list', help='Classes to train (otherwise all classes are used)')
        parser.add_argument('--data-limit', type=int, help='Max size fot the dataset')
        parser.add_argument('--data-size', help='Size of the data (will be resized and padded)')
        parser.add_argument('--data-path', help='Path to the dataset main folder')
        parser.add_argument('--data-pickle', help='Path to the folder containing the numpy files')
        parser.add_argument('--output-dir', nargs='?', default='output',
                            help='Data directory where output files will be saved')
        arguments = parser.parse_args()

        class_list = arguments.class_list.split(',') if arguments.class_list else None
        data_size = [int(size) for size in arguments.data_size.split(',')] if arguments.data_size else None
        pickle_name = os.path.join(
            arguments.data_pickle, '{}'.format(arguments.dataset)) if arguments.data_pickle else None
        if pickle_name:
            if class_list:
                pickle_name += '_class' + '_'.join(class_list)
            if data_size:
                pickle_name += '_size_' + '_'.join([str(size) for size in data_size])
            if arguments.data_limit:
                pickle_name += '_limit_{}'.format(arguments.data_limit)
        if arguments.data_pickle and os.path.exists(pickle_name + '.hdf5'):
            logger.info('Loading data from files {}'.format(pickle_name))
            with h5py.File(pickle_name + '.hdf5', 'r') as h5_file:
                data_count = h5_file['data_count'][0]
                train_data = []
                train_labels = []
                for data_index in range(data_count):
                    train_data.append(h5_file['data_{}'.format(data_index)][:])
                    train_labels.append(h5_file['label_{}'.format(data_index)][:])
                train_data = np.asarray(train_data)
                train_labels = np.asarray(train_labels)
            # train_data = np.load(pickle_name + '_data.npy')
            # train_labels = np.load(pickle_name + '_labels.npy')
        else:
            if not arguments.data_path:
                logger.error('--data-path is needed if --data-pickle is not set nor found')
            if arguments.dataset == 'pascal_voc_2012':
                from src.dataset.pascal_voc_2012 import PascalVoc2012
                logger.info('Loading labels from PascalVoc2012 folder : {}'.format(arguments.data_path))
                train_data, train_labels = PascalVoc2012.load_data(
                    arguments.data_path, class_list, data_limit=arguments.data_limit,
                    resize=data_size, min_size=500, logger=logger)

                if arguments.data_pickle:
                    logger.info('Writing data in file : {}'.format(pickle_name + '.hdf5'))
                    os.makedirs(arguments.data_pickle, exist_ok=True)
                    with h5py.File(pickle_name + '.hdf5', 'w') as h5_file:
                        h5_file.create_dataset('data_count', data=[len(train_labels)])
                        for index, data in enumerate(train_data):
                            h5_file.create_dataset('data_{}'.format(index), data=train_data[index])
                            h5_file.create_dataset('label_{}'.format(index), data=train_labels[index])
                    # np.save(pickle_name + '_data.npy', train_data)
                    # np.save(pickle_name + '_labels.npy', train_labels)
            else:
                logger.error('Dataset not implemented')
                exit(1)

        os.makedirs(arguments.output_dir, exist_ok=True)

        logger.info("Data shape : %s" % str(train_data.shape))
        os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
        train(train_data, train_labels, arguments, logger=logger)
    except Exception as error:
        logger.error(''.join(traceback.format_exception(*sys.exc_info())))
        raise error


if __name__ == '__main__':
    main()
